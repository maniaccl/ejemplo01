function processSuccess_guardaObsLocal(data, status, req) 
{
   
   $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        	},
        	message: 'Procesando..' 
     		});//fin blockUi
      if (status == "success"){
		if ($(req.responseXML).find("status").text() == 'OK')
		{
	        //$("#response").text($(req.responseXML).find("status").text());
         	/*sessvars.id_user = $(req.responseXML).find("id_usr").text();
         	sessvars.username=$("#rut").val();
         	sessvars.password=$("#pass").val();
			sessvars.$.flush();*/
			$.unblockUI();//saca el msg "Procesando...".
			alert("Datos Ingresados.");
			history.back();
		 	//window.location.href="campanas.html";
		}else{
			alert("Error al subir imagen,favor sacar foto nuevamente.");
			sessvars.error = 1;
			sessvars.error_login = 1;
			sessvars.$.flush();
			//window.location.href="error.html";
		}
    }
    //alert(status);
}//fin processSuccess.

function processError_guardaObsLocal(data, status, req)
{
	//sessvars.error = 2;
	sessvars.detalle_error=($(data.responseText).find("faultstring").text());
	sessvars.codigo_error=($(data.responseText).find("faultcode").text());
	sessvars.error_login = 1;
	sessvars.$.flush();
  //window.location.href="error.html";
  //alert(($(data.responseText).find("faultstring").text())+'Sin Conexión...Intente Nuevamente');//muestra error que trae el WS.
  alert('Error al subir imagen...Intente Nuevamente');//muestra error que trae el WS.
  location.reload();
} 