var wsUrl = "http://url.dominio/server.php?wsdl";//para probar de afuera.

function showAlert(msj)
{
    navigator.notification.alert(
        msj,  // message
        '',   // title
        ''    // buttonName
    );
}//fin function mensaje.

function guarda()
{
	var sw = document.getElementById('sw');
	
	if(sw.value==0)
		showAlert('No ha capturado foto!');
	else
		showAlert('Foto Guardada!');
}
//funcion que guarda observaciones local en la bd
function guardaObsLocal(id_reg,id_local)
{
	var adjunto='';
	var id_adjunto='';
	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        	},
        	message: 'Procesando..' 
     });//fin blockUi
     
	var obs='';
	obs=$("#observaciones option:selected").text();
	adjunto=sessvars.nom_imagen;
	
	var soapRequest ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:loc="http://www.csapi.org/schema/wsconsulta/v2_2/local"><soapenv:Header/><soapenv:Body><loc:observLocal><loc:datos><id_local_campana_circuito>'+id_reg+'</id_local_campana_circuito><observaciones>'+obs+'</observaciones><otros>'+otros+'</otros><adjunto>'+adjunto+'</adjunto><id_adjunto>'+id_adjunto+'</id_adjunto><id_local>'+id_local+'</id_local></loc:datos></loc:observLocal></soapenv:Body></soapenv:Envelope>';
			//alert(soapRequest);
            $.ajax({
                type: "POST",
                url: wsUrl,
                contentType: "text/xml",
                dataType: "xml",
                data: soapRequest,
                processData: false,
                success: processSuccess_guardaObsLocal,
                error: processError_guardaObsLocal
            });

		 return true;	
}//fin function guardaObsLocal.
